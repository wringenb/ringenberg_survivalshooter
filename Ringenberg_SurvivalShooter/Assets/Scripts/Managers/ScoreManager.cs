﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    int highScore;


    Text text;


    void Awake ()
    {
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        print("High Score: " + highScore);

        text = GetComponent <Text> ();

        score = 0;
    }


    void Update ()
    {
        text.text = "Score: " + score;

        if (score > highScore)
        {
            highScore = score;
            PlayerPrefs.SetInt("HighScore", highScore);
            print("NEW HIGH SCORE!");
        }
    }
}
